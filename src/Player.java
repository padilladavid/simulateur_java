import java.util.ArrayList;
import java.util.List;

public class Player {
	private String name;
	private float cash;
	private int sales;
	private float profit;
	private List<Recipe> recipes = new ArrayList<Recipe>();
	
	public Player(String n, float c, int s, float p) {
		name = n;
		cash = c;
		sales = s;
		profit = p;
	}
	
	public void addRecipe(Recipe r) {
		recipes.add(r);
	}
	
	public String getName() {
		return name;
	}
	public float getCash() {
		return cash;
	}
	public float getProfit() {
		return profit;
	}
	public int getSales() {
		return sales;
	}
	public List<Recipe> getRecipes() {
		return recipes;
	}
}
