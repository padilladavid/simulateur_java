import org.json.*;

public class JSONFactory {
	private Game game;
	
	public JSONFactory(Game g) {
		game = g;
	}
	
	public void getPlayers(JSONObject json) {
		JSONObject map = json.getJSONObject("map");
		JSONArray players = map.getJSONArray("ranking");
		for(int i=0; i<players.length(); i++) {
			JSONObject playerInfo = map.getJSONObject("playerInfo").getJSONObject(players.getString(i));
			Player p = new Player(players.getString(i), (float)playerInfo.getDouble("cash"), playerInfo.getInt("sales"), (float)playerInfo.getDouble("profit"));
			JSONArray drinks = map.getJSONObject("drinkByPlayer").getJSONArray(p.getName());
			for(int j=0; j<drinks.length(); j++) {
				p.addRecipe(new Recipe(drinks.getJSONObject(j).getString("name"), (float)drinks.getJSONObject(j).getDouble("price")));
			}
			game.addPlayer(p);
		}
	}
	
	public void setTime(JSONObject json) {
		
	}
}
