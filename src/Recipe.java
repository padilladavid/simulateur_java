
public class Recipe {
	private String name;
	private float price;
	private int salesNumber;
	
	public Recipe(String n, float p) {
		name = n;
		price = p;
		salesNumber = 0;
	}
	
	public String getName() {
		return name;
	}
	public int getSalesNumber() {
		return salesNumber;
	}
	public void setSalesNumber(int salesNumber) {
		this.salesNumber = salesNumber;
	}
	public float getPrice() {
		return price;
	}
}
