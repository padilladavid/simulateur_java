import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class Game {
	private long timestamp;
	private enum Weather {
		HEATWAVE,
		SUNNY,
		CLOUDY,
		RAINY,
		THUNDERSTORM
	}
	Weather meteo;
	private List<Player> players = new ArrayList<Player>();
	
	public void addPlayer(Player p) {
		players.add(p);
	}
	
	public JSONObject makeSales() {
		JSONArray salesArray = new JSONArray();
		for(int i=0; i<players.size(); i++) {
			JSONObject sale = new JSONObject();
			sale.put("player", players.get(i).getName());
			List<Recipe> recipes = players.get(i).getRecipes();
			for(int j=0; j<recipes.size(); j++) {
				sale.put("item", recipes.get(i).getName());
				sale.put("quantity", recipes.get(i).getSalesNumber());
			}
			salesArray.put(sale);
		}
		return (new JSONObject()).put("sales", salesArray);
	}
	
	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public static void main(String[] args) {
		Game game = new Game();
		Network net = new Network();
		JSONFactory jf = new JSONFactory(game);
		try {
			long previous = -1;
			while(true) {
				JSONObject time = net.getTemps();
				if(game.getTimestamp() != previous) {
					JSONObject map = net.getMap();
					jf.getPlayers(map);
					JSONObject sales = game.makeSales();
					net.postSales(sales);
				}
				previous = game.getTimestamp();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
